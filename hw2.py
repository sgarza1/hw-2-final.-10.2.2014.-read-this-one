# ---------------------------------------------------- #
# File: hw2.py
# ---------------------------------------------------- #
# Author(s): your_name(s), with BitBucket handle (name)
# Stephen Garza
# Monica Parucho 
# BitBucket handle: sgarza1
# BitBucket link: https://bitbucket.org/sgarza1/hw2-9.30.2014/commits/fda7aaab1dc33da1151fab3f5f07037740e60c4e
# ---------------------------------------------------- #
# Plaftorm:    Stephen-Unix Monica-Windows
# Environment: Python 2.7.8
# Libaries:    numpy 1.9,0
#              matplotlib
#       	   scipy 0.14.0
#			   skimage 0.10.1
#			   cv2 2.4.9
#			   
#              
#       	 
# ---------------------------------------------------- #
# Description: Fulfills hw2 requirements
# ---------------------------------------------------- #



import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import scipy.ndimage as ndimage
import scipy.misc as misc
from glob import glob
import time
from matplotlib.path import Path
from skimage.feature import corner_harris, corner_subpix, corner_peaks
import cv2


#corner code from http://scikit-image.org/docs/dev/auto_examples/plot_corner.html
#flow code from canvas example

class Triangle:

	def __init__(self, height=0, width=0): # The parameters height, width, area aren't called in main()
		self.height = height
		self.width = width
		self.area = self.getArea()

	def getArea(self): # This function is not called in main()
		return self.height * self.width / 2

	def IsInside(self,tri_path,testpoint):
		ans = tri_path.contains_point(testpoint)

		return ans

	def draw_triangle(self,sigma): 
		
		sq_im = np.zeros((256,256))

		# vertix on +y axis
		yy3= 128
		xx3= np.random.randint(10,128) #created a 10-space safety net to prevent triangle from rotating out of bounds
		# vertix on 3rd quadrant
		yy1, xx1 = np.random.randint(10,128), np.random.randint(128,246)
		# vertix on 4th quadrant
		yy2, xx2 = np.random.randint(128,246), np.random.randint(128,246)

		vertex1= np.array([yy1,xx1])
		vertex2= np.array([yy2,xx2])
		vertex3= np.array([yy3,xx3])

		#print vertex1, vertex2, vertex3

		tri_path= Path([[xx1,yy1],[xx2,yy2],[xx3,yy3],[xx1,yy1]])

		sq_im[xx3,yy3] = 1
		sq_im[xx1,yy1] = 1
		sq_im[xx2,yy2] = 1

		i=0
		j=0

		for i in range (0,256):
			for j in range (0,256):

				testpoint = np.array([i,j])
				ans = self.IsInside(tri_path,testpoint)

				if ans == 1:
					sq_im[i][j]=1
				else:
					sq_im[i][j]=0

		blurry_im = ndimage.gaussian_filter(sq_im,sigma)
		
		plt.figure(figsize=(10,5))
		plt.imshow(blurry_im, cmap=plt.cm.gray, interpolation = 'nearest')
		plt.title('Random Triangle')
		plt.axis('off')
		plt.show()

		self.clear_pic=sq_im
		self.blur_pic=blurry_im



	def save_rotated_images(self,im,deg,step,iterations): #spins triangle. Saves images of rotated triangles
		
		for counter in range(iterations):

			self.rotate_im=ndimage.rotate(im,(step*counter), reshape=False)
			misc.imsave('pics%02d.png' % counter, self.rotate_im)
			

	def get_edges(self,im): #finds the edges inside an image. 
		sx = ndimage.sobel(misc.imread(im),axis=1)
		sy = ndimage.sobel(misc.imread(im),axis=0)
		sob = np.hypot(sx, sy)
		plt.imshow(sy, cmap=plt.cm.gray)
		plt.title('Edges from a rotated image')
		plt.axis('off')
		plt.show()


	def save_corner_images(self,iterations): #uses harris filter. Saves images with vertices overlayed on triangle
		#harris filter preformed well for sigma = 0 
		#performance is worse for sigma = 2. Occasionally can't detect all corners. 
		
		global dummy_coords
		dummy_coords = []

		for counter in range(iterations):
			image=('pics%02d.png'%counter)

			coords = corner_peaks(corner_harris(misc.imread(image)), min_distance=5)#default min distance=5
			dummy_coords.append(coords)
			plt.hold('on')
			if len(coords)==3:
				plt.plot(coords[0, 1], coords[0, 0], '.r', markersize=20)
				plt.plot(coords[1, 1], coords[1, 0], '.r', markersize=20)
				plt.plot(coords[2, 1], coords[2, 0], '.r', markersize=20)
			if len(coords)==2:
				plt.plot(coords[0, 1], coords[0, 0], '.r', markersize=20)
				plt.plot(coords[1, 1], coords[1, 0], '.r', markersize=20)
			else:
				plt.plot(coords[0, 1], coords[0, 0], '.r', markersize=20)

			plt.imshow(misc.imread(image), interpolation='nearest', cmap=plt.cm.gray)
			
			
			plt.hold('off')
			plt.axis('off')
			plt.savefig('corner%02d.png'%counter)
			plt.clf()



	def get_binary_image(self,im,sigma=0): #displays a single binary image

		#doesn't work well when sigma=2. Works better with sigma=0
		

		binary_im = ndimage.binary_opening(im)
		binary_im = ndimage.gaussian_filter(binary_im,sigma)
		plt.imshow((im-binary_im),cmap=plt.cm.gray)
		plt.title('Vertices from Original Triangle: Binary Filter')
		plt.axis('off')
		plt.show()

	def update_animation(self,ii): #reads in pics w/o corners marked
		im=plt.imshow(misc.imread('pics%02d.png' % ii),cmap=plt.cm.gray)
		plt.axis('off')
		plt.title('Rotating Triangle')
		return im
		

	def update_corner_animation(self,ii):#reads in pics w/ corners marked

		im=plt.imshow(misc.imread('corner%02d.png' % ii),cmap=plt.cm.gray)
		plt.axis('off')
		plt.title('Vertice Tracker')
		return im
		


	def animate(self,iterations,interval=1): #images images w/o marked corners
		fig=plt.figure()
		ani = animation.FuncAnimation(fig,tri.update_animation, frames=range(iterations), interval=interval, repeat=True)
		#plt.show()
		# plt.axis('off')
		# plt.title('Rotating Triangle')
		
		ani.save('ani_rotate.mp4') #this would save your animation to your cd. I do not have the software to save mp4's from sublim
		plt.show()
	def animate_vertices_harris(self,iterations,interval=1): #using this as for optical flow.
		fig=plt.figure()
		ani = animation.FuncAnimation(fig,tri.update_corner_animation, frames=range(iterations), interval=interval, repeat=True)
	

		ani.save('ani_rotate_vertices.mp4') #this would save your animation to your cd. I do not have the software to save mp4's from sublim
		plt.show()

		#this could be sped up by decreasing the interval size
		#this could be sped up by imputing images directly instead of saving them and loading them back in
		#this is done using the harris method to track the vertices in each image.
		#once tracked, I simply plotted the string of images to show their paths

	def plot_waves(self, dummy_coords):

		global tvar
		tvar = np.arange(1,25,1)
		#print tvar

		xpoints = []
		ypoints = []

		# print dummy_coords[0][0][0]
		# print dummy_coords[1][0][0]
		# print dummy_coords[0][1][0]
		# print dummy_coords[0][0][1]

		for i in range (0,36):
			for j in range (0,2):

				xpoints.append(dummy_coords[i][j][0])

		for i in range (0,36):
			for j in range (0,2):

				ypoints.append(dummy_coords[i][j][1])

		# print xpoints
		# print ypoints
		# print len(xpoints)
		# print len(ypoints)

		xpv1=xpoints[::3]
		xpv2=xpoints[1::3]
		xpv3=xpoints[2::3]

		ypv1=ypoints[::3]
		ypv2=ypoints[1::3]
		ypv3=ypoints[2::3]

		#print xpv1
		# print len(xpv1)
		# print len(tvar)

		plt.subplot(2,1,1)
		plt.plot(tvar,xpv1,'r^',tvar,xpv2, 'g^',tvar, xpv3,'y^')
		plt.title('X-point vs. Time from Vertices')
		plt.xlabel('Time')
		plt.ylabel('X-value')

		plt.subplot(2,1,2)
		plt.plot(tvar,ypv1,'r^',tvar,ypv2, 'g^',tvar, ypv3,'y^')
		plt.title('Y-point vs. Time from Vertices')
		plt.xlabel('Time')
		plt.ylabel('Y-value')

		plt.show()


def main():

	deg=360 #specifies final degree of rotation
	step=10 #step size in degrees. Determines how many images to take
	iterations=deg/step+1 #number of images. Include an extra for 0 deg rotation

	sigma=int(raw_input("Enter 0 or 2 for Gaussian filter parameter."))
	print ''
	size=64 #size of triangle
	interval=10 #will be angular velocity of animation. Inversely proportional to rotational speed
	

	opition=raw_input("Enter 'f' for fast angular velocity. Enter 's' for slow angular velocity: ")
	if opition=='s':
		interval=1000
	if opition=='f':
		interval=10

	global tri
	tri=Triangle(4,5) #initalizes triangle obect. The height and width do not influde the image or problem at all
	
	print '"x" out of figures/animations to close them.'
	print 'Loading...'
	tri.draw_triangle(sigma) 
	tri.save_rotated_images(tri.blur_pic,deg,step,iterations)
	tri.animate(iterations,interval)
	
	tri.get_edges('pics01.png')
	tri.get_binary_image(tri.clear_pic) #finds vertices from a binary arrary

	print 'Loading...'	

	tri.save_corner_images(iterations)#takes rotated images and overlays vertices on them.
	tri.animate_vertices_harris(iterations,interval)
	tri.plot_waves(dummy_coords)
	
main()

######################################################################################################################################
def draw_flow(img, flow, step=16):
    h, w = img.shape[:2]
    y, x = np.mgrid[step/2:h:step, step/2:w:step].reshape(2,-1)
    fx, fy = flow[y,x].T
    lines = np.vstack([x, y, x+fx, y+fy]).T.reshape(-1, 2, 2)
    lines = np.int32(lines + 0.5)
    vis = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    cv2.polylines(vis, lines, 0, (0, 255, 0))
    for (x1, y1), (x2, y2) in lines:
        cv2.circle(vis, (x1, y1), 1, (0, 255, 0), -1)
    return vis

def draw_hsv(flow):
    h, w = flow.shape[:2]
    fx, fy = flow[:,:,0], flow[:,:,1]
    ang = np.arctan2(fy, fx) + np.pi
    v = np.sqrt(fx*fx+fy*fy)
    hsv = np.zeros((h, w, 3), np.uint8)
    hsv[...,0] = ang*(180/np.pi/2)
    hsv[...,1] = 255
    hsv[...,2] = np.minimum(v*4, 255)
    bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    return bgr

def warp_flow(img, flow):
    h, w = flow.shape[:2]
    flow = -flow
    flow[:,:,0] += np.arange(w)
    flow[:,:,1] += np.arange(h)[:,np.newaxis]
    res = cv2.remap(img, flow, None, cv2.INTER_LINEAR)
    return res



def main2():

    cap = cv2.VideoCapture('ani_rotate_vertices.mp4')
    
    ret, prev = cap.read()
    prevgray = cv2.cvtColor(prev, cv2.COLOR_BGR2GRAY)
    show_hsv = False
    show_glitch = False
    cur_glitch = prev.copy()

    while True:
        ret, img = cap.read()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        flow = cv2.calcOpticalFlowFarneback(prevgray, gray, 0.5, 3, 15, 3, 5, 1.2, 0)
        prevgray = gray

        cv2.imshow('flow', draw_flow(gray, -flow))
        if show_hsv:
            cv2.imshow('flow HSV', draw_hsv(flow))
        if show_glitch:
            cur_glitch = warp_flow(cur_glitch, flow)
            cv2.imshow('glitch', cur_glitch)

        ch = 0xFF & cv2.waitKey(5)
        if ch == 27:
            break
        if ch == ord('1'):
            show_hsv = not show_hsv
            print 'HSV flow visualization is', ['off', 'on'][show_hsv]
        if ch == ord('2'):
            show_glitch = not show_glitch
            if show_glitch:
                cur_glitch = img.copy()
            print 'glitch is', ['off', 'on'][show_glitch]
    
    cv2.destroyAllWindows()
    

main2()




