READ ME.
 
The code you pulled from this repository my be out of date. I was having trouble pushing from terminal. The code under my .zip file in confluence will for sure be up to date.

Thank you. 


Data Generation
Function draw_triangle(sigma): Window = zeros(256,256)
vertex1= randominteger from 128 to 236, randominteger from 20 to 128 vertex2= randominteger from 128 to 236, randominteger from 128 to 236 vertex3= randominteger from 20 to 128, 128
triangle= Path([vertex1àvertex2àvertex3àvertex1])
for i in range (0,256):
for j in range (0,256):
testpoint = [i,j] ans=IsInside(triangle,testpoint) if ans == 1:
window[i][j]=1 else:
window[i][j]=0
Function IsInside(triangle, testpoint):
Ans= triangle.contains_point(testpoint) Return ans
Function save_rotated_images(im,deg,step,iterations): For i in range(iterations)
Rotate_im = ndimage.rotate(image, step*i, reshape = false)
Misc.imsave(‘pics%02d.png’, % i, self.rotate_im) End
Function update_animation(ii):
Im = plt.imshow(misc.imread('pics%02d.png' % ii),cmap=plt.cm.gray) Return im
Function animate(iterations,interval=10): Fig=plt.figure()
Ani = animation.funcanimation(fig, tri.update_animation)
Feature Identification
Function get_binary_image(im, sigma = 0): Binary_im=ndimage.binary_opening(im) Binary_im=ndimage.gaussian_filter(binary_im,sigma)
Function get_edges(im):
Sx = ndimage.sobel(misc.imread(im), axis = 1) Sy = ndimage.sobel(misc.imread(im), axis = 0) Sob = hypot(sx, sy)
Plot(Sy)
Function save_corner_images(iterations): For i in range(iterations)
Image = (‘pics%02d.png’% i)
coords = corner_peaks(corner_harris(misc.imread(image)), min_distance = 5) plot(misc.imread(image))
im=plot(coords[0,1], coords[0,0], ‘.y’, markersize = 1)
im=plot(coords[1,1], coords[1,0], ‘.g’, markersize = 1)
im=plot(coords[2,1], coords[2,0], ‘.r’, markersize = 1)
save('corner%02d.png'% i )
Vertice Tracking
Function update_corner_animation(ii):
Im = plt.imshow(misc.imread('corner%02d.png' % ii),cmap=plt.cm.gray) Return im
Function animate_vertices_harris(iterations, interval= 10): Fig=plt.figure()
Ani=animation.funcanimation(fig,tri.update_corner_animation)
Optical Flow
Majority of code taken from canvas example